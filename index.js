import { NativeModules } from 'react-native';

const { LocalNotification } = NativeModules;

export default LocalNotification;
