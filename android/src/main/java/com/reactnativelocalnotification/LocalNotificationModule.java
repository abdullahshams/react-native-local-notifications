package com.reactnativelocalnotification;

import android.content.Context;
import android.widget.Toast;
import android.app.Notification.Builder;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.NotificationChannel;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

public class LocalNotificationModule extends ReactContextBaseJavaModule {
    NotificationManager notificationManager;
    private final ReactApplicationContext reactContext;

    public LocalNotificationModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;
    }

    @Override
    public String getName() {
        return "LocalNotification";
    }

    @ReactMethod
    public void sampleMethod(String stringArgument, int numberArgument, Callback callback) {
        // TODO: Implement some actually useful functionality
        callback.invoke("Received numberArgument: " + numberArgument + " stringArgument: " + stringArgument);
    }
    @ReactMethod
        public void show(String text) {
        Context context = getReactApplicationContext();
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        
        String channelId = "Your_channel_id";
        NotificationChannel channel = new NotificationChannel(
                                        channelId,
                                        "Channel human readable title",
                                        NotificationManager.IMPORTANCE_HIGH);

        Notification noti = new Notification.Builder(context)
         .setContentTitle("Testing")
         .setContentText("This the test subject")
         .setSmallIcon(R.drawable.ic_launcher)
         .setChannelId(channelId)
         .build();
         notificationManager.createNotificationChannel(channel);
         notificationManager.notify(43,noti);
    }
}
